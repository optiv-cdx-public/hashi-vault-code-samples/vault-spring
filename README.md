# Java Spring Example

**Note**: The documentation for the Spring Vault library isn't great and hasn't been updated lately, therefore you sometimes need to dig to figure out how to do certain things. There are a lot of good examples hidden inside their src/test/documentation directories: https://github.com/spring-projects/spring-vault/tree/main/spring-vault-core/src/test/java/org/springframework/vault/documentation

This repository is meant to demonstrate some of the basic functionality of HashiCorp Vault. Specifically, it demonstrates storing and accessing secrets stored in HashiCorp's KV2 (key/value pair) engine. 

This repository demonstrates the following Vault functionality:

- How to connect a Java application to a running instance of Vault
- How to write a key/value secret to Vault
- How to read a key/value secret from Vault

---

## Java & Spring Installation

If you do not have Java/Spring installed on your machine already, please follow the instructions below to install them.

1) Connect to your Linux instance 

2) Install Java JDK

    Depending on what platform you're running, installing Java is a different process. 

    - Amazon Linux
   
        ```bash
        sudo amazon-linux-extras install java-openjdk11
        ```

    To see which version of java is running type: `java --version`

3) Install SDK & Spring

    Install SDK (Software Development Kit Manager)

    ``` bash
    curl -s "https://get.sdkman.io" | bash
    source "$HOME/.sdkman/bin/sdkman-init.sh"
    ```

    Verify that SDK was installed properly by running:

    ```bash
    sdk version
    ```

    If SDK was installed properly, you should see something similar to the following:

    ```
    > SDKMAN 5.11.5+713
    ```

    Use SDK to install the latest version of Spring

    ```bash
    sdk install springboot
    ```

    If installation was successful, you will see the following:

    ```bash
    Installing: springboot 2.5.1
    Done installing!

    Setting springboot 2.5.1 as default.
    ```
---

## Add SpringVault to an existing project

To install the SpringVault into an existing project, add one of the following to your project:

- Maven
    ```bash
    <dependency>
        <groupId>org.springframework.vault</groupId>
        <artifactId>spring-vault-core</artifactId>
        <version>${version}</version>
    </dependency>
    ```

To view additional documentation / how-to's for VaultSpring, check out their github repository located here: https://github.com/spring-projects/spring-vault

---

## Configure your environment to connect to Vault

Set the following environment variables to configure which Vault server this application connects to. These variables tell your app where your instance of Vault is running, which token to use for authentication and which namespace to use.

```bash
export VAULT_ADDR='http://127.0.0.1:8200'
export VAULT_TOKEN='[TOKEN]'
export VAULT_NAMESPACE=''
```

---

## Create a test secrets engine

Run the following to create a test secrets engine we can read from and write to:

```
vault secrets enable --path=test kv-v2
```

---

## Run the Java Spring app

Build and run the sample Java app. You will see output when the operations perfomed are successful.

```bash
cd java_spring
./mvnw install && java -jar target/demo-0.0.1-SNAPSHOT.jar
```

If the Spring app started successfull, you should see the following output:

```bash

  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::                (v2.5.1)

2021-06-14 16:12:59.759  INFO 7167 --- [       runner-0] o.s.boot.SpringApplication               : Starting application using Java 11.0.11 on ip-172-31-24-136.us-east-2.compute.internal with PID 7167 (started by ec2-user in /home/ec2-user/java_spring)
2021-06-14 16:12:59.764  INFO 7167 --- [       runner-0] o.s.boot.SpringApplication               : No active profile set, falling back to default profiles: default
2021-06-14 16:13:01.104  INFO 7167 --- [       runner-0] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat initialized with port(s): 8080 (http)
2021-06-14 16:13:01.120  INFO 7167 --- [       runner-0] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
2021-06-14 16:13:01.120  INFO 7167 --- [       runner-0] org.apache.catalina.core.StandardEngine  : Starting Servlet engine: [Apache Tomcat/9.0.46]
2021-06-14 16:13:01.158  INFO 7167 --- [       runner-0] org.apache.catalina.loader.WebappLoader  : Unknown class loader [org.springframework.boot.cli.compiler.ExtendedGroovyClassLoader$DefaultScopeParentClassLoader@5bfbf16f] of class [class org.springframework.boot.cli.compiler.ExtendedGroovyClassLoader$DefaultScopeParentClassLoader]
2021-06-14 16:13:01.193  INFO 7167 --- [       runner-0] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
2021-06-14 16:13:01.193  INFO 7167 --- [       runner-0] w.s.c.ServletWebServerApplicationContext : Root WebApplicationContext: initialization completed in 1135 ms
2021-06-14 16:13:01.905  INFO 7167 --- [       runner-0] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
2021-06-14 16:13:01.917  INFO 7167 --- [       runner-0] o.s.boot.SpringApplication               : Started application in 2.768 seconds (JVM running for 5.132)
```

You can now navigate to http://localhost:8200 to launch the Vault Spring demo.

If the web app ran successfull, you should see the following output in your browser:

```bash
Vault is AWESOME! :)
```

Then in your terminal, you should see the following output:

```bash
Using Vault: http://localhost:8200
Writing secret: test/data/demo
Writing data:
item1: adp is awesome
item2: spring is awesome
item3: protect your secrets
Reading secret: test/data/demo
item1: adp is awesome
item2: spring is awesome
item3: protect your secrets
Writing secret: test/data/demo
Writing data:
item1: adp is awesome
item2: updated secrets are neat
item3: this is neat
Reading secret: test/data/demo
item1: adp is awesome
item2: updated secrets are neat
item3: this is neat
```

To validate the KV was written successfully, you can also:

- Login to the Vault UI: http://127.0.0.1:8200/ui
- Verify via the command line `vault kv get test/demo`
