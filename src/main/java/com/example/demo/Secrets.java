package com.example.demo;

public class Secrets {
    
    public String item1;
    public String item2;
    public String item3;

    public Secrets() {}

    public void setItem1(String item1) {
        this.item1 = item1;
    }

    public void setItem2(String item2) {
        this.item2 = item2;
    }

    public void setItem3(String item3) {
        this.item3 = item3;
    }

    public String getItem1() {
        return this.item1;
    }

    public String getItem2() {
        return this.item2;
    }

    public String getItem3() {
        return this.item3;
    }
}
