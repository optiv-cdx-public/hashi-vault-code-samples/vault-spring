
package com.example.demo;
import java.util.Collections;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.vault.authentication.TokenAuthentication;
import org.springframework.vault.client.VaultEndpoint;
import org.springframework.vault.core.VaultOperations;
import org.springframework.vault.core.VaultTemplate;
import org.springframework.vault.support.VaultResponse;
import org.springframework.vault.support.VaultResponseSupport;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@ComponentScan("org.springframework.vault")
public class DemoApplication {

	private VaultOperations vaultOperations;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
	
	@GetMapping("/")
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {

		VaultEndpoint vaultEndpoint = VaultEndpoint.create("localhost", 8200);
		vaultEndpoint.setScheme("http");

		System.out.println("Using Vault: " +  vaultEndpoint.toString());

		vaultOperations = new VaultTemplate(vaultEndpoint, new TokenAuthentication(System.getenv("VAULT_TOKEN")));

		String secretName = "test/data/demo";

		Secrets secrets = new Secrets(); 
		secrets.setItem1("adp is awesome");
		secrets.setItem2("spring is awesome");
		secrets.setItem3("protect your secrets");

		writeKV2Secret(secretName, secrets);

		readKV2Secret(secretName);

		secrets.setItem2("updated secrets are neat");
		secrets.setItem3("this is neat");

		writeKV2Secret(secretName, secrets);

		readKV2Secret(secretName);

		return "Vault is AWESOME! :)";
	}

	private void writeKV2Secret(String secretName, Secrets secrets) {

		System.out.println("Writing secret: " + secretName);
		System.out.println("Writing data: ");
		System.out.println("item1: " + secrets.getItem1());
		System.out.println("item2: " + secrets.getItem2());
		System.out.println("item3: " + secrets.getItem3());

		vaultOperations.write(secretName, Collections.singletonMap("data", secrets));
	}

	private void readKV2Secret(String secretName) {

		System.out.println("Reading secret: " + secretName);

		VaultResponse read = vaultOperations.read(secretName);
		Map<String, String> data = (Map<String, String>) read.getRequiredData().get("data");

		System.out.println("item1: " + data.get("item1"));
		System.out.println("item2: " + data.get("item2"));
		System.out.println("item3: " + data.get("item3"));
	}

}
            